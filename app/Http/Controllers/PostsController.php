<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    // public function __constructor()
    // {
    //     $this->middleware( 'auth' );
    //     //$this->middleware( 'auth', [ 'only' => [ 'add' ] ] );

    // }

    // Create new post
    public function createPost( Request $request )
    {
        $post = Post::create( $request->all() );

        return response()->json( $post );
    }

    // Update post
    public function updatePost( Request $request, $id )
    {
        $post = Post::find( $id );
        $post->title = $request->input( 'title' );
        $post->body = $request->input( 'body' );
        $post->views = $request->input( 'views' );
        $post->save();

        return response()->json( $post );
    }

    // Delete post
    public function deletePost( $id )
    {
        $post = Post::find( $id );
        $post->delete();

        return response()->json( 'Removed successfully' );
    }

    // List posts
    public function index()
    {
        $post = Post::all();

        return response()->json( $post );
    }

    // Get Post
    public function viewPost( $id )
    {
        $post = Post::find( $id );

        return response()->json( $post );
        //echo 'me';
    }
}
